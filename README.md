Jawframe
========


Basing our first framework edition on Symfony3, we think that this is a good option.

This is because we only use what we _REALLY_ need for our web development project(s).


Plans
=====

We plan to keep the same file structure the the upstream framework uses. I believe we 
have a good start without creating extra problems somewhere down the road.